* sphinxcontrib-proof 1.1.0 (2018-10-17)

    * Internationalisation: Custom theorem types can be set in configuration file (thanks Dominic Charrier). Closes #2.
    * Custom theorem types can be added in the configuration files.
    * Theorems can be labelled, referenced, numbered (and several related new configuration options). Closes #3.
    * More options for the LaTeX build.
    * Add python 3.7 support.
    * Internally, rename `name` to `label`, and `content` to `name`.
    * Add `singlehtml` builder. Closes #5.

    -- Louis Paternault <spalax+python@gresille.org>

* sphinxcontrib-proof 1.0.1 (2017-01-01)

    * Fix wheel.

    -- Louis Paternault <spalax+python@gresille.org>

* sphinxcontrib-proof 1.0.0 (2017-12-30)

    * Drop python2 support.
    * Minor internal improvements.

    -- Louis Paternault <spalax+python@gresille.org>

* sphinxcontrib-proof 0.1.1 (2017-02-19)

    * Add python3.6 support.
    * Fix minor bugs introduced by sphinx1.5.
    * Minor test improvements.

    -- Louis Paternault <spalax+python@gresille.org>

* sphinxcontrib-proof 0.1.0 (2015-09-26)

    * First published version: works for both LaTeX and HTML builders

    -- Louis Paternault <spalax+python@gresille.org>
